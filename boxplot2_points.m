function boxplot2_points(X,Y,thick,printval)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Modified from boxplot2 by R. Pavao
% This version also plots the sorted points behind the box plots as in the
% article Beckert, Pavao, and Pena (2017)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% help from original boxplot2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% BOXPLOT2 plots median (red, quantile 0.5), interquartile range (blue,
% quantiles 0.25 and 0.75) and non-outliers range (black, quantiles 0.05
% and 0.95)
%
% Syntax: boxplot2(X,Y)
%
% Example:
%      X=1:2;
%      Y{1}=1:100;
%      Y{2}=101:200;
%      subplot(311), boxplot2(X,Y)     ,  xlim([0 3])
%      subplot(312), boxplot2(X,Y,0.4) ,  xlim([0 3])
%      subplot(313), boxplot2(X,Y,0.2) ,  xlim([0 3])
%
% Check boxplot3 (mean and standard error)
%
% Bugs? rpavao@gmail.com
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for i=1:length(Y)
    Y{i}(isnan(Y{i}))=[];
end

if nargin<2
    help boxplot3
    return
end

if nargin<3
    l=max(cellfun(@length,Y));
    it=linspace(-0.5,0.5,l);
    low=floor(0.25*l);
    high=floor(0.75*l);
    thick=it(high)-it(low);
end
if nargin<4
    printval=0;
end
xlim([0 X(end)+2])
plot(xlim,[0 0],'k--')
hold on

Y=cellfun(@sort,Y,'UniformOutput',0);
for d=1:length(X)
    plot(X(d)+linspace(-0.5,0.5,length(Y{d})),Y{d},'k.','Markersize',2)
end

for g=1:length(X)
    data=Y{g};
    yvalues=quantile(data,[0.05 0.25 0.5 0.75 0.95]);
    xvalues=thick*[-0.5 +0.5]+X(g);
    xvalues2=0.5*thick*[-0.25 +0.25]+X(g);
    plot(xvalues,[1 1]*yvalues(3),'color',[0.7 0 0],'linewidth',1)
    plot([xvalues(1) xvalues(2) xvalues(2) xvalues(1) xvalues(1)],[yvalues(2) yvalues(2) yvalues(4) yvalues(4) yvalues(2)],'color',[0 0 0.7],'linewidth',1)
    plot([1 1]*X(g),yvalues(1:2),'k')
    plot(xvalues2,[1 1]*yvalues(1),'k')
    plot([1 1]*X(g),yvalues(4:5),'k')
    plot(xvalues2,[1 1]*yvalues(5),'k')

    if printval
        for v=1:5
            text(thick*+printval+X(g),yvalues(v),[num2str(yvalues(v)) ' '],'FontSize',7,'HorizontalAlignment','right','VerticalAlignment','middle')
        end
    end
end
end