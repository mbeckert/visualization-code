function quartile_error_plot(data, x, col)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% This function will perform a line plot of x vs. median of data along with
% a background patch denoting the first and third quartiles
%
% "data" should be in a cell format with values
% "x" is the x values desired, if nothing is assigned default will be the
%   length of the data cell vector
% "col" is the desired line and background color. Default is red
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if ~exist('x', 'var')
    x = 1:length(data);
end
if ~exist('col', 'var')
    col = 'r';
end

plot(x, cellfun(@nanmedian, data), col);
hold on

yvalues = cell2mat(cellfun(@(x) quantile(x, [0.25 0.75])', data, 'UniformOutput', 0));
yvalues = [yvalues(1,:), fliplr(yvalues(2, :))];
xvalues = [x fliplr(x)];

ax = fill(xvalues, yvalues, col);
set(ax, 'facealpha', 0.25, 'edgecolor', 'w')

end